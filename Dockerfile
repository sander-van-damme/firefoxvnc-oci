FROM registry.fedoraproject.org/fedora-minimal as novnc

RUN microdnf install -y git
RUN git clone --depth 1 https://github.com/novnc/noVNC.git /novnc
RUN rm -rf /novnc/.git
RUN git clone --depth 1 https://github.com/novnc/websockify.git /novnc/utils/websockify
RUN rm -rf /novnc/utils/websockify/.git


FROM registry.fedoraproject.org/fedora-minimal
LABEL version="3" maintainer="Sander Van Damme"

RUN microdnf install -y sway wayvnc firefox-wayland procps-ng libXt && microdnf clean all -y
COPY --from=novnc /novnc /usr/share/novnc

RUN mkdir /config && chmod a=rwx,o+t /config

COPY ./init.sh /init.sh

EXPOSE 5900
EXPOSE 8080

CMD ["/bin/bash", "/init.sh"]
